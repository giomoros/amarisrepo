<?php /* Template Name: family-support */ ?>
<?php include('header.php'); ?>
<body style="overflow-x: hidden;">
<?php include('navbar.php'); ?>

  <!--==========================
    Intro Section

  ============================-->

  

<section class="header-position" id="team" style="background-image:url('<?php echo esc_url( $template_directory_uri . '/wp-img/family-support-bg.jpg"' ); ?>');">

    <div class="container" style="">
      <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
       
         <h3 class="text-white title-tag" style="font-size: 60px;">Family <span class="text-white sub-title-tag" style="font-size: 60px;">Support</span></h3>
       
      </div>
    </div>
      <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
         <span class="ml-3 mr-3 hb-1"></span>
         <span class="ml-3 mr-3 hb-2"></span>
         <span class="ml-3 mr-3 hb-3"></span>
          </div>
          
  </section><!-- #intro -->

  <section id="call-to-action" style="background-image: none;">
    <div class="container pt-5" style="max-width:1000px">
      <div class="row">
      <div class="col-md-4">
       <div class="row pt-3 justify-content-end" style="width:100%;">
         <span class="ml-3 mr-3" style="height: 5px;width:100%;background: #8c5776"></span>
          </div>
        <h4 class="pt-2 text-dark font-tofino" style="max-width: 80%;font-weight: 700;">Mentor Program</h4>
        <p  style="max-width: 80%;font-weight: 500;" class="text-dark font-tofino">As many of us at Amaris have personal experience with adoption, we understand the complexity of the process and the isolation that can occur after. Because of this, we have developed a mentor program where we match new adoptive families with other families who have already adopted. This provides support for the process, but also creates community for the families and the adopted children to know other families that “look like theirs”.</p>
          <div class="row text-center" style="padding: 0px 20px;">
            <a href="<?php echo site_url( $path, $scheme ); ?>/donate" class="btn btn-outline-default btn-sm mt-2" style="border-color: #666 !important;color:#666;border-radius: 1.2rem;width:120px;border-width: 3px;"><b>Register now</b></a> 
          </div>
      </div>

       <div class="col-md-4">
         <div class="row pt-3 justify-content-end" style="width:100%;">
         <span class="ml-3 mr-3" style="height: 5px;width:100%;background: #fbb26a"></span>
          </div>
         <h4 class="pt-2 text-dark font-tofino" style="max-width: 80%;font-weight: 700">Resprite Program</h4>
         <p style="max-width: 80%;font-weight: 500;" class="text-dark font-tofino">Adoption can be challenging, especially if you have opened your home to an older child or a child who has come with significant trauma or unhealthy attachment history. Because of this, we have compiled a list of families throughout Alberta who are willing to step up for a couple of days to take in your child and offer you a bit of respite and rest. These families are skilled in dealing with children with special needs and are also adoptive families themselves. If you feel like you are reaching the end of your rope, please reach out and access our respite program...we can all use a bit of help and rest once in a while!</p>

           <div class="row text-center" style="padding: 0px 20px;">
            <a href="<?php echo site_url( $path, $scheme ); ?>/donate" class="btn btn-outline-default btn-sm mt-2" style="border-color: #666 !important;color:#666;border-radius: 1.2rem;width:120px;border-width: 3px;"><b>Register now</b></a> 
          </div>

      </div>

      <div class="col-md-4">
       <div class="row pt-3 justify-content-end" style="width:100%;">
         <span class="ml-3 mr-3" style="height: 5px;width:100%;background: #fbb26a"></span>
          </div>
        <h4 class="pt-2 text-dark font-tofino" style="max-width: 80%;font-weight: 700">Age-out Program</h4>
        <p style="max-width: 80%;font-weight: 500" class="text-dark font-tofino">The majority of young people who grow up in government care end up ‘aging out’ of the system at 18 years with little to no informal support. Although they occasionally have social workers and maybe some funding, everyone, no matter how old you are, needs someone to count on and turn to. Our age out program matches young people who have aged out of care with caring families who can offer somewhere to go on holidays and an opportunity to receive unconditional love. Although this is not an official adoption program, it is up to the young person if they would eventually like to pursue an adult adoption once they have spent significant time with the family. All families in this program have been screened and supported and the majority of them have also adopted children who are now grown. If you are a young person who is looking for a family, or a familiy willing to open your home, please click below </p>
          <div class="row text-center" style="padding: 0px 20px;">
            <a href="<?php echo site_url( $path, $scheme ); ?>/donate" class="btn btn-outline-default btn-sm mt-2" style="border-color: #666 !important;color:#666;border-radius: 1.2rem;width:120px;border-width: 3px;"><b>Register now</b></a> 
          </div>
      </div>

    </div>
    </div>
  </section>




  <section style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url( $template_directory_uri . '/wp-img/happy-hour.png"' ); ?>')" id="team">

  <div class="row justify-content-end full-width" style="position:relative;top:18%;left:0%;height:100%;width:100%">
  <img style="width:40%;z-index: 1;position:absolute;top:39%;right:-15px;" src="<?php echo esc_url( $template_directory_uri . '/wp-img/tag.png"' ); ?>">

</div>

</section>





<section id="team" style="background: #202020;min-height:40%;width: 90%; margin: 0 0 0 auto;position: relative;top:-80px;">
    
<div class="container pb-1" style="padding: 85px 60px ;">
  <h3 class="text-white" style="font-family: tofino;font-weight: 500">Special Considerations</h3>
  <p class="text-white" style="max-width: 880px;">At Amaris, we are committed to finding a home for EVERY child, regardless of history, special need, age, gender, race or any other factor. Because of this commitment, we have started a list of exceptional families with special hearts and additional training who are open to children with unique needs. If you are interested in learning more about being on this list, please contact our office </p>


 <div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>



           <h3 class="text-white mt-5">Education</h3>
  <p class="text-white" style="max-width: 880px;">At Amaris, we believe that information and education is crucial to a successful adoption.You can never do enough reading, research, talking to other families, attending conferences, or accessing online training. Because of this value, we are always ready and willing to support your learning through answering questions or sending you specific resources. We also regularly keep our families informed through our newsletter about upcoming educational opportunities to enhance your skills and learning around adoption. Although most of the education we offer is optional, there are a few mandatory courses that you will need to complete prior to us signing off on your home study.</p>

   <p class="text-white" style="max-width: 880px;">There is a training requirement for domestic adoptions of a minimum of 6 hours of online training. We require the following courses prior to your home study.</p>

   <div class="pl-5 text-white font-tofino " style="font-weight: 500">
    1. Open Adoption 101<br>
    2. Let's Talk Adoption<br>
    3. The Journey of Attachment<br>
   </div>

   <p class="text-white mt-4" style="max-width: 880px;">These courses can be accessed at Adoption Learning Partners<br><span style="color:#9bcefa">http://www.adoptionlearningpartners.org/adopting/domestic-adoption.cfm</span></p>

</div>


<div class="container-fluid p-5" style="width:90%;margin-left: 30px;padding-bottom: 100px;">
  <div class="row">
      <div class="col-md-3 pl-0 pr-0">
       <div class="row pt-3" style="width:100%;">
        <h1 class="mb-0 text-white font-tofino" style="font-size: 60px;line-height: 0.9"><b>1</b></h1>
         <span class="" style="height: 15px;width:100%;background: #fff"></span>
         
        <h4 class="pt-2 text-white font-tofino" style="font-weight: 700;font-size: 14px;">International Adoption <br>Training</h4>
        <p style="max-width: 85%;font-size: 14px;" class="text-white">one day mandatory training for any family moving through the process of an international adoption. Topics include; process, attachment, trauma, grief and loss, and a presentation from one or two families who have recently adopted internationally. This training is offered every month, alternating between a Calgary and Edmonton location. Please see the calendar for the next training in your area.</p>
          
      </div>
       </div>

       <div class="col-md-3 pl-0 pr-0">
         <div class="row pt-3 " style="width:100%;">
          <h1 class="mb-0 text-white font-tofino"style="font-size: 60px;line-height: 0.9"><b>2</b></h1>
         <span class="" style="height: 15px;width:100%;background: #fff"></span>
         
         <h4 class="pt-2 text-white font-tofino" style="font-weight: 700;font-size: 14px;">Domestic Adoption </h4>
         <p style="max-width: 85%;font-size: 14px;" class="text-white">two separate day mandatory training for families pursuing a domestic adoption. Topics include: process, open adoption; attachnment, grief and loss, prenatal exposure and a presentation from a birth parent and/or adoptive family. This training is offered twice a year so make sure you check our calendar to register. If you have attended domestic training in the past, you are welcome to attend any subsequent training sessions free of charge to update your knowledge.</p>

           
        </div>
      </div>

      <div class="col-md-3 pl-0 pr-0">
       <div class="row pt-3 " style="width:100%;">
        <h1 class="mb-0 text-white font-tofino"style="font-size: 60px;line-height: 0.9"><b>3</b></h1>
         <span class="" style="height: 15px;width:100%;background: #fff"></span>
        
        <h4 class="pt-2 text-white font-tofino" style="font-weight: 700;font-size: 14px;">Relative Adoption <br>Training</h4>
        <p style="max-width: 85%;font-size: 14px;" class="text-white">if you are adopting a relative, we offer mandatory training tailored to your unique situation. Please contact our office to register</p>
         
      </div>
      </div>

<div class="col-md-3 pl-0 pr-0">
       <div class="row pt-3" style="width:100%;">
        <h1 class="mb-0 text-white font-tofino"style="font-size: 60px;line-height: 0.9"><b>4</b></h1>
         <span class="" style="height: 15px;width:100%;background: #fff"></span>
        
        <h4 class="pt-2 text-white" style="font-weight: 700;font-size: 14px;">Empowered to Connect Conference</h4>
        <p style="max-width: 85%;font-size: 14px;" class="text-white">every year we encourage all adoptive families to register for the Empowered to Connect conference which is offered in various locations around Alberta. Please sign up for our newsletters to stay updated on this, and other, relevant conferences, such at Belong, Together For Adoption, Christian Alliance for Orphans, NACAC, and others..</p>
          
      </div>
      </div>

    </div>
  </div>
    


  </section>

  <section id="team" class="pb-5 pt-2">
    <div class="container" >
      <div class="row pt-3 offset-1">
         <span class="ml-3 mr-3" style="height: 5px;width:22.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:22.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:22.6666%;background: #f3dd8a"></span>
          </div>
      <div class="offset-1 pl-3 mt-3 pt-4" style="max-width: 800px">
<h3 class=" mt-2" style="color:#000"><b>Counselling</b></h3>
<p class="fw-500 mt-3"style="color:#000">There isn’t anyone, at any stage of life, who could not benefit from counseling. There is always room for each of us to understand ourselves and others more and grow in our compassion and ability to support others. At Amaris, we offer different types of counseling based on your particular need:</p>

<div class="pl-5">
  <p class="fw-500" style="color:#000"> Free counseling to expectant or birth parents considering an adoption plan for their child</p>
   <p class="fw-500" style="color:#000">Mandatory counseling for adoptive couples who are applying to adopt a child 3 years of age or older or with unique special needs</p>
   <p class="fw-500" style="color:#000">Ongoing support and resource counseling available to any family who may be struggling</p>
   <p class="fw-500" style="color:#000">Free consultations for adoptive families considering adoption and wanting to know their options</p>
  </div>

</div>
    </div>
  </section>


<?php include('footer.php'); ?>
