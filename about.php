<?php /* Template Name: about */ ?>
<?php include('header.php'); ?>
<body style="overflow-x: hidden;">

  <?php include('navbar.php'); ?>

  <!--==========================
    Intro Section

  ============================-->

  

<section class="header-position" id="team" style="background-image:url('<?php echo esc_url( $template_directory_uri . '/wp-img/about-bg.jpg"' ); ?>');">

    <div class="container" style="">
      <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
        <h3 class="text-white title-tag" style="font-size: 60px;">Our  <span style="font-size: 60px;right:-20px;" class="sub-title-tag">Mission</span></h3>
       


      </div>
    </div>
      <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
          <span class="ml-3 mr-3 hb-1"></span>
         <span class="ml-3 mr-3 hb-2"></span>
         <span class="ml-3 mr-3 hb-3"></span>
          </div>
          
  </section><!-- #intro -->




    
  
  

<section id="team" style="padding: 60px 15px !important;">
     <div class="row pt-3" >
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>

    <div class="container" style="max-width: 860px;>
      <div class="offset">
<h3 class="mt-2 font-tofino text-dark"><b>Why we exist.</b></h3>
<p class=" mt-4 text-dark"><b>Amaris exists to restore family. </b>
 
</p>


<p class=" mt-4 font-tofino fw-500 text-dark">We believe that children are precious, that children are meant to flourish within a loving family, and that every child is worthy of the support and care that a family can give. We exist to support adoptive parents, guide expecting parents exploring adoption for their baby, walk with new adoptive families, and strengthen families needing support. We empower you to make informed choices for your family while always keeping the wellbeing of the child as our first priority. 
</p>

</div>


    </div>
  </section>


  <section id="team" style="background: #202020;padding: 15px;padding-bottom: 60px;">
     <div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>

    <div class="container-fluid" style="max-width: 860px;padding-bottom: 50px">
      <div class="offset">
<h3 class="text-white mt-2 "><b>Who we are.</b></h3>
<p class="text-white mt-3">Amaris, formerly Christian Adoption Services, was founded in 1989 as a non-profit adoption agency,licensed by the Government of Alberta under the Child, Youth, and Family Enhancement Act to provide professional adoption and family support services to residents of Alberta. With experienced accredited social workers in Calgary, Edmonton, Red Deer, Lethbridge, LaCrete, and Grand Prairie we are able to serve a wide range of communities, both urban and rural, throughout Alberta. Contact us to inquire about your location.</p>

</div>

<div class="offset mt-5 pt-4">
<h3 class="text-white mt-2 "><b>What we do.</b></h3>
<p class="text-white mt-3">Amaris guides families before, during and after adoption while focusing on meeting the needs of the diverse children and clients we serve. We provide professional, ethical, compassionate, personal, and effective support services for families of all shapes and sizes. We facilitate domestic adoption and assist with international adoption. </p>

</div>
    </div>
  </section>

  <section id="team" style="padding: 60px 15px !important;">
     <div class="row pt-3" >
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>

    <div class="container" style="max-width: 860px;>
      <div class="offset">
<h3 class="mt-2 font-tofino text-dark"><b>Why Amaris?</b></h3>
<p class=" mt-4 font-tofino fw-500 text-dark">Previously “Christian Adoption Services,” the heart has always been a child focused model of adoption. The word Amaris means “given by God” and “you are loved”; which we believe to be true of every child. These words reflect the heart of everything that we do. We hold this at the forefront throughout the adoption process and every interaction we have. And, because our process is built around the wellbeing of each child, we were compelled to call ourselves by a name that reflects those deeply held values. 
</p>

</div>


    </div>
  </section>


<section style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url( $template_directory_uri . '/wp-img/family-1.jpg"' ); ?>')" id="team">

  <div class="row justify-content-end full-width" style="position:relative;top:0%;left:0%;height:100%;width:100%">
  <img style="width:45%;z-index: 1;position:absolute;top:33%;right:-130px;" src="<?php echo esc_url( $template_directory_uri . '/wp-img/tag.png"' ); ?>">

</div>

</section>



  <section id="team" style="background: #202020;padding: 15px;padding-bottom: 60px;width: 90%; margin: 0 0 0 auto;position: relative;top:-100px">
     <!--<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>-->

    <div class="container-fluid" style="max-width: 1000px;padding: 50px 0px">
      <div class="offset">
<h3 class="text-white mt-2 "><b>We believe/we are.</b></h3>
<p class="text-white mt-3">Belonging Through Family

</p>

<p class="text-white mt-3">We believe in <span class="fw-500">empowering</span> individuals by helping you to make informed decisions about your family. We believe in family, and the reality of unconditional love wherever you are. We believe in <span class="fw-500">compassion</span>; experiencing a safe place filled with understanding and acceptance. Our passion and motivation comes from our <span class="fw-500">faith</span>; all that we do emerges from our belief that every child is created in God’s image and worthy of love, dignity, hope, and family. Within all of this we place great value on <span class="fw-500">openness</span>; emphasizing child focused collaboration through open relationships.</p>

</div>
<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
          </div>

<div class="offset mt-3 pt-4">
<h3 class="text-white mt-2 "><b>Not your typical adoption agency.</b></h3>
<p class="text-white mt-3">Amaris is unique in that we aim to empower the parent and focus on the child. We embrace the tension that comes with adoption, and the emotions this process can evoke. Knowing adoption can simultaneously be both the most beautiful and the most difficult journey for someone to embark on, we walk alongside our clients every step of the way with compassion and understanding and without any judgement. Our staff have a knowledge of adoption which comes out of their personal adoption journeys as well as certified training. With Amaris, you will have the understanding and support you need throughout your adoption process and beyond.
</p>

</div>
    </div>
  </section>



  <section id="team" style="padding: 0px 0px;width: 90%; margin: 0 0 0 auto;">


    <div class="container-fluid" style="max-width: 1000px;padding: 0px 0px">

     <div class="row">
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
     </div>

     <div class="offset" style="padding: 0px 15px;padding-bottom: 15px;padding-top: 65px;" >
      <div class="row pl-3">
       <h3 class="mt-2" style="width: 100%;color:#000"><b>Team</b></h3><br>
       <p class="mt-4" style="color:#000;font-weight: 600">Our strength is our people. We care.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1">
          <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-1.png"' ); ?>" alt="Snow" style="width:80%;">
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500""><b>Tandela Swann</b></span>
           </div>
         </div>
       </div>
       <p class="ml-4" style="color:#8c5776b5;font-weight: 600">Executive Director</p>
        </div>



     <div class="col-md-3">
       <div class="tab1">
         <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-2.png"' ); ?>" alt="Snow" style="width:80%;">
         <div class="bottomleft" style="background: #fbb26ad1;height: 30%;">
          <div class="tab1">
           <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500""><b>Kimberley Gee</b></span>
         </div>
       </div>
     </div>
     <p class="ml-4" style="color:#fbb26a;"><b>Program Director</b> <i>Domestic Adoptions</i></p>
   </div>



   <div class="col-md-3">
     <div class="tab1">
       <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-3.png"' ); ?>" alt="Snow" style="width:80%;">
       <div class="bottomleft" style="background: #f3dd8ad6;height: 30%;">
        <div class="tab1">
         <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500""><b>Kathy Williams</b></span>
       </div>
     </div>
   </div>
   <p class="ml-4" style="color:#f3dd8a;"><b>EProgram Director</b> <i>Edmonton</i></p>

 </div>


 <div class="col-md-3">
   <div class="tab1">
     <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-4.png"' ); ?>" alt="Snow" style="width:80%;">
     <div class="bottomleft" style="background: #555555bf;height: 30%;">
      <div class="tab1">
       <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Sandy Bassen</span>
     </div>
   </div>
 </div>
 <p class="ml-4" style="color:#555555;font-weight: 600">Office Manager</p>

</div>
</div>

</div>




<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
      
       <p class="mt-4" style="color:#000;font-weight: 600">Lethbridge Social Workers.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1">
          <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-5.png"' ); ?>" alt="Snow" style="width:80%;">
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Quincy Palmer</span>
           </div>
         </div>
       </div>
       
     </div>
    </div>
</div>

<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
       
       <p class="mt-4" style="color:#000;font-weight: 600">Calgary Social Workers.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1 mt-2 mb-2">
          <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-6.png"' ); ?>" alt="Snow" style="width:80%;">
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Mylene Hensch</span>
           </div>
         </div>
       </div>
      
     </div>



     <div class="col-md-3">
       <div class="tab1  mt-2 mb-2">
         <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-7.png"' ); ?>" alt="Snow" style="width:80%;">
         <div class="bottomleft" style="background: #fbb26ad1;height: 30%;">
          <div class="tab1">
           <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Sterna Venter</span>
         </div>
       </div>
     </div>
    
   </div>



   <div class="col-md-3">
     <div class="tab1  mt-2 mb-2">
       <!--<img src="<?php echo esc_url( $template_directory_uri . '/wp-img/team.png"' ); ?>" alt="Snow" style="width:80%;">-->
       <div class="blank-bg"></div>
       <div class="bottomleft" style="background: #f3dd8ad6;height: 30%;">
        <div class="tab1">
         <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Timena Osborne</span>
       </div>
     </div>
   </div>
   
 </div>


 <div class="col-md-3">
   <div class="tab1  mt-2 mb-2">
     <!--<img src="<?php echo esc_url( $template_directory_uri . '/wp-img/team.png"' ); ?>" alt="Snow" style="width:80%;">-->
     <div class="blank-bg"></div>
     <div class="bottomleft" style="background: #555555bf;height: 30%;">
      <div class="tab1">
        <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Amanda Spate</span>
     </div>
   </div>
 </div>
 

</div>
</div>

</div>


<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
       
       <p class="mt-4" style="color:#000;font-weight: 600">Red Deer Social Workers.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1  mt-2 mb-2">
          <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-10.png"' ); ?>" alt="Snow" style="width:80%;">
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Lori Frank</span>
           </div>
         </div>
       </div>
      
     </div>

</div>

</div>


<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
       
       <p class="mt-4" style="color:#000;font-weight: 600">Edmonton Social Workers.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1  mt-2 mb-2">
          <!--<img src="<?php echo esc_url( $template_directory_uri . '/wp-img/team.png"' ); ?>" alt="Snow" style="width:80%;">-->
          <div class="blank-bg"></div>
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Leanne Kohn</span>
           </div>
         </div>
       </div>
      
     </div>

      <div class="col-3">
        <div class="tab1  mt-2 mb-2">
          <!--<img src="<?php echo esc_url( $template_directory_uri . '/wp-img/team.png"' ); ?>" alt="Snow" style="width:80%;">-->
          <div class="blank-bg"></div>
          <div class="bottomleft" style="background: #fbb26ad1;height: 30%;">
            <div class="tab1">
            <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Marcia Peters</span>
           </div>
         </div>
       </div>
    
     </div>

</div>

</div>


<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
       
       <p class="mt-4" style="color:#000;font-weight: 600">Grande Prairie Social Worker.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1  mt-2 mb-2">
          <img src="<?php echo esc_url( $template_directory_uri . '/wp-img/tm-13.png"' ); ?>" alt="Snow" style="width:80%;">
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span class="font-tofino" style="position: absolute;top:10px;left:10px;font-weight: 500">Jodi Lozcyc</span>
           </div>
         </div>
       </div>
       
     </div>

</div>

</div>


<div class="offset" style="padding: 0px 15px;padding-bottom: 15px;" >
      <div class="row pl-3">
       
       <p class="mt-4" style="color:#000;font-weight: 600">LaCrete Social Worker.</p>

     </div>

     <div class="row mb-2 mt-2">
       <div class="col-md-3">
        <div class="tab1  mt-2 mb-2">
          <!--<img src="<?php echo esc_url( $template_directory_uri . '/wp-img/team.png"' ); ?>" alt="Snow" style="width:80%;">-->
          <div class="blank-bg"></div>
          <div class="bottomleft" style="background: #8c5776b5;height: 30%;">
            <div class="tab1">
             <span style="position: absolute;top:10px;left:10px;">Leah Martens</span>
           </div>
         </div>
       </div>
     
     </div>

</div>

</div>




</div>
</div>
</section>




  <!--==========================
    Footer
  ============================-->
  <?php include('footer.php'); ?>