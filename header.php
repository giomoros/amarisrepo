<!DOCTYPE html>
<html lang="en" style="margin-top:0px !important;">
<head>
 <?php $template_directory_uri = get_template_directory_uri(); ?>

 <script>

 </script>

<style>
body
{
  font-family: tofino !important;
 font-weight: normal;
letter-spacing: 1px !important

}

.font-tofino
{
  font-family: tofino;
}
.prog-align
{
width: 60px;
height: 3px !important;
border-radius: 20px;
position: relative;
bottom:18px;
left:37%

}
.dashed
{
  width:22px;
  height: 5px;
  background-color: #fff;
  border-radius: 40px;
  margin: 10px;
}
.fw-500
{
  font-weight: 500;
}
.text-dark
{
  color:#000 !important;
}
.member:hover
{
  opacity: 0.8;
  transition: 0.3s;
}


.hash-fix
{
     width: 35%;
    position: absolute;
    margin-top: -76px;
}

#header.header-scrolled {
    background: rgba(0, 0, 0, 0.9);
    padding: 15px 0 !important;
    height: 80px !important;
    transition: all 0.5s;
}
.member1 {
    position: relative;
    text-align: center;
    color: white;
}
.tab1 {
    position: relative;
}
.centered {
    position: absolute;
    top: 50%;
    left: 58%;
    transform: translate(-72%, -50%);
}

.bottomleft {
    position: absolute;
    bottom: 0px;
    left: 0px;
    width:100%;
    padding: 10px;
    font-size: 22px;
    font-weight: 600;
    color:#fff;


  }
  .blank-bg
  {
    height: 176px;
    width:176px;
    background-color: #707070
  }

.hb-1
{
 height: 20px;
 width:16.6666%;
 background: #8c5776;
position:relative;
top:5px;
}
.hb-2
{
 height: 20px;
 width:16.6666%;
 background: #fbb26a;
 position:relative;
top:5px;
}

.hb-3
{
 height: 20px;
 width:16.6666%;
 background: #f3dd8a;
 position:relative;
top:5px;
}

.header-position
{
  background-position: 10% 45% !important;
  background-attachment: fixed !important;
  background-size: cover !important;
  height: 100vh;
}

  @font-face {
  font-family: Altous;
   src: url(<?php echo esc_url( $template_directory_uri . '/font/emmeline-regular-webfont.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/emmeline-regular-webfont.woff2' ) ?>);


  font-weight: normal;
}

@font-face {
  font-family: tofino-black;
   src:url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BlackItalic.woff' ) ?>),
       url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BlackItalic.woff2') ?>);
        font-weight: bold;
  }

@font-face {
  font-family: tofino;
   src:url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Regular.woff') ?>),

  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BlackItalic.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Bold.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BoldItalic.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Book.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BookItalic.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Light.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-LightItalic.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-MediumItalic.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino Reg Subset WOFF/Tofino-Black.woff' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino Reg Subset WOFF/Tofino-Black.woff2' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BlackItalic.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Bold.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BoldItalic.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Book.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-BookItalic.woff2' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Light.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-LightItalic.woff2' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-MediumItalic.woff2' ) ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Regular.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),
  url(<?php echo esc_url( $template_directory_uri . '/font/Tofino-Medium.woff2') ?>),


  
}


.title-tag
{
font-size: 41px;
font-family:tofino;
font-weight: normal !important;

}


.sub-title-tag
{
font-size: 55px;
font-family:Altous;
position: relative;
top: 30px;
}


</style>
<script>
window.onscroll = function() {
  growShrinkLogo()
};

function growShrinkLogo() {
  var Logo = document.getElementById("myLogo")
  if (document.body.scrollTop > 70 || document.documentElement.scrollTop > 5) {
    Logo.style.width = '40px';
    Logo.style.transition = '0.5s';
  } else {
    Logo.style.width = '150px';
  }



var Menues = document.getElementById("Menues")
  if (document.body.scrollTop > 70 || document.documentElement.scrollTop > 5) {
  Menues.style.marginTop  = '0px';
  Menues.style.transition = '0.5s';
 } else {
   Menues.style.marginTop  = '20px';
  }

  var Navscroll = document.getElementById("myNavbar")
 if (document.body.scrollTop > 70 || document.documentElement.scrollTop > 5) {
  Navscroll.style.position  = 'relative';
  Navscroll.style.bottom  = '5px';
  Navscroll.style.transition = '0.5s';
} else {
 Navscroll.style.marginTop  = '0px';
 }

 //nav-menu-container


}
</script>

  <meta charset="utf-8">
  <title>Amaris</title>
<!--<meta content="width=device-width, initial-scale=1.0" name="viewport">-->
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=1">

  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700"" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/bootstrap/css/bootstrap.min.css"' ); ?> " rel="stylesheet">

 <link rel="icon" href="<?php echo esc_url( $template_directory_uri . '/wp-img/favicon.png"' ); ?>"  type="image/png" sizes="96x96">
  
  <!-- Libraries CSS Files -->
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/font-awesome/css/font-awesome.min.css"' ); ?> " rel="stylesheet">
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/animate/animate.min.css"' ); ?> " rel="stylesheet">
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/ionicons/css/ionicons.min.css"' ); ?>" rel="stylesheet">
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/owlcarousel/assets/owl.carousel.min.css"' ); ?>" rel="stylesheet">
  <link href="<?php echo esc_url( $template_directory_uri . '/lib/lightbox/css/lightbox.min.css"' ); ?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?php echo esc_url( $template_directory_uri . '/css/style.css"' ); ?>  " rel="stylesheet">
<link href="<?php echo esc_url( $template_directory_uri . '/style.css"' ); ?>  " rel="stylesheet">
  <!-- =======================================================
    Theme Name: Amaris
    Theme URL: https://bootstrapmade.com/Amaris-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
  
  <?php wp_head(); ?>
  
</head>
