<?php

class CodeBase_Theme
{
    

    
    public function my_theme_enqueue_styles()
    {
		wp_enqueue_style('lightbox-styling', get_stylesheet_directory_uri().'/lib/lightbox/css/lightbox.min.css');
       		
    }

    
	public function __construct()
    {
        
        add_action('wp_enqueue_scripts', array($this, 'my_theme_enqueue_styles'));
       
	}
	

	
	}

$theme = new CodeBase_Theme();
