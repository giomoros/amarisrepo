<!--==========================
  Header
============================-->

<style>

    .nav-menu a {
        padding: 0 20px 0px 8px !important;
    }

    .progress {
        background-color: #fff !important;
    }

    .topbarheight {
        height: 3px !important;
        margin-bottom: 0px !important;
    }

    .hover-line {
        height: 3px;
        position: relative;
        left: 10px;
        margin-top: 2px;
    }

    #mobile-nav-toggle {
        position: fixed;
        right: 0;
        top: 0;
        z-index: 999;
        margin: 20px 20px 0 0;
        border: 0;
        background: none;
        font-size: 24px;
        display: none;
        transition: all 0.4s;
        outline: none;
        cursor: pointer;
    }

    @media (max-width: 768px) {
        #nav-menu-container {
            display: none;
        }

    }

    @media (max-width: 768px) {
        #mobile-nav-toggle {
            display: inline !important;
        }

        .res-hide {
            display: none;
        }

        .nav-menu > li {
            float: unset !important;
        }

        .bg-black-res {
            background: rgba(0, 0, 0, 0.7);

        }

        .ptpb {
            padding-bottom: 10px;
            padding-top: 10px;
        }

    }

    body.mobile-nav-active #mobile-nav {
        left: 0 !important;
    }

    #mobile-nav {
        position: fixed;
        top: 0;
        padding-top: 18px;
        bottom: 0;
        z-index: 998;
        background: rgba(0, 0, 0, 0.8);
        left: -260px;
        width: 260px;
        overflow-y: auto;
        transition: 0.4s;
    }

    @media (min-width: 1024px) {
        .res-hide-desktop {
            display: none;
        }
    }

    @media (min-width: 800) {
        .res-hide-desktop {
            display: none;
        }
    }

    .donate-btn {
        border-color: #fff !important;
        color: #fff;
        border-radius: 2.2rem;
        float: right;
        border-width: 3px;
        width: 80px;
        text-transform: uppercase;
    }

    .donate-btn:hover {
        color: #fff !important;
        background-color: #dfc0c3
    }

    .donate-active {
        color: #fff !important;
        background-color: #dfc0c3
    }

    nav ul li a:hover, a:focus {
        color: #fff !important;
        text-decoration: none !important;
    }


</style>
<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#myNavbar"
        id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>


<header id="header">
    <div class="container">

        <div class="row ">

            <div style="width:100%" class="pr-0  text-center linkact">
                <a href="<?php echo site_url($path, $scheme); ?>/index" class="scrollto res-hide"><img id="myLogo"
                                                                                                       style="width:150px;position: relative;left: 35px;"
                                                                                                       src="<?php echo esc_url($template_directory_uri . '/wp-img/logo.png"'); ?>"></a>
                <a href="<?php echo site_url($path, $scheme); ?>/index" class="scrollto res-hide-desktop"><img
                            id="myLogo2" style="width:100px;position: relative;bottom:5px;"
                            src="<?php echo esc_url($template_directory_uri . '/wp-img/logo.png"'); ?>"></a>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->

                <?php if ($pagename != 'donate') { ?>
                    <a href="<?php echo site_url($path, $scheme); ?>/donate"
                       class="btn btn-outline-default btn-sm mt-2 res-hide donate-btn">Donate</a>
                <?php } ?>

                <?php if ($pagename == 'donate') { ?>
                    <a href="<?php echo site_url($path, $scheme); ?>/donate"
                       class="btn btn-outline-default btn-sm mt-2 res-hide donate-btn"
                       style="background-color: #dfc0c3;color:#fff">Donate</a>
                <?php } ?>


            </div>


            <!--<div class="justify-content-end">
          <a href="#" class="btn btn-outline-default btn-sm mt-2" style="border-color: #fff !important;color:#fff;border-radius: 1.2rem;float: left">Learn More</a>
      </div>-->
        </div>

        <?php $current_url = $_SERVER['REQUEST_URI'];
        $pagename = "home";
        if (strpos($_SERVER['REQUEST_URI'], 'adopting') !== false) {
            $pagename = 'adopting';
        } else if (strpos($_SERVER['REQUEST_URI'], 'index') !== false) {
            $pagename = 'home';

        } else if (strpos($_SERVER['REQUEST_URI'], 'about') !== false) {
            $pagename = 'about';

        } else if (strpos($_SERVER['REQUEST_URI'], 'expecting') !== false) {
            $pagename = 'expecting';

        } else if (strpos($_SERVER['REQUEST_URI'], 'story') !== false) {
            $pagename = 'story';

        } else if (strpos($_SERVER['REQUEST_URI'], 'family-support') !== false) {
            $pagename = 'family-support';
        } else if (strpos($_SERVER['REQUEST_URI'], 'donate') !== false) {
            $pagename = 'donate';
        }


        ?>


        <div id="Menues" class="row justify-content-center bg-black-res"
             style="margin-top:30px;position: relative;bottom: 7px;">
            <div id="myNavbar" class="res-hide">
                <nav id="nav-menu-container pr-0" class="ptpb">

                    <ul class="nav-menu">
                        <li class="one"><a href="<?php echo site_url($path, $scheme); ?>/index">Home</a>
                            <?php if ($pagename == 'home') { ?>
                                <div class="progress topbarheight"></div>
                                <div class="progress hover-line"></div>
                            <?php } ?>
                        </li>


                        <li class="two"><a href="<?php echo site_url($path, $scheme); ?>/about">About</a>
                            <?php if ($pagename == 'about') { ?>
                                <div class="progress topbarheight"></div>
                                <div data-wow-delay="0.1s" class="progress animated slideInLeft hover-line"></div>
                            <?php } ?>
                        </li>


                        <li class="three"><a href="<?php echo site_url($path, $scheme); ?>/adopting">Adopting</a>

                            <?php if ($pagename == 'adopting') { ?>
                                <div class="progress topbarheight"></div>
                                <div data-wow-delay="0.1s" class="progress  wow slideInLeft hover-line"></div>
                            <?php } ?>

                        </li>
                        <li class="four"><a href="<?php echo site_url($path, $scheme); ?>/expecting">expecting</a>

                            <?php if ($pagename == 'expecting') { ?>
                                <div class="progress topbarheight"></div>
                                <div data-wow-delay="0.1s" class="progress wow slideInLeft hover-line"></div>
                            <?php } ?>
                        </li>


                        <li class="five"><a href="<?php echo site_url($path, $scheme); ?>/story">stories</a>
                            <?php if ($pagename == 'story') { ?>
                                <div class="progress topbarheight"></div>
                                <div data-wow-delay="0.1s" class="progress wow slideInLeft hover-line"></div>
                            <?php } ?></li>


                        <li class="six"><a href="<?php echo site_url($path, $scheme); ?>/family-support">family
                                Support</a>
                            <?php if ($pagename == 'family-support') { ?>
                                <div class="progress topbarheight"></div>
                                <div data-wow-delay="0.1s" class="progress  wow slideInLeft hover-line"></div>
                            <?php } ?></li>

                        <li class="res-hide-desktop">
                            <a href="<?php echo site_url($path, $scheme); ?>/donate" class="btn btn-outline-default btn-sm mt-2
     " style="border-color: #fff !important;color:#fff;border-radius: 1.2rem;">Donate</a>
                        </li>

                        <div class="underbar"></div>

                    </ul>

                </nav><!-- #nav-menu-container -->
                <div id="mobile-body-overly"></div>
            </div>
        </div>


    </div>
</header><!-- #header -->