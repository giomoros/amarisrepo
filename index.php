<?php include('header.php'); ?>
    <body style="overflow-x: hidden;">
<?php include('navbar.php'); ?>

    <!--==========================
      Intro Section

    ============================-->


    <section class="header-position" id="team"
             style="background-image:url('<?php echo esc_url($template_directory_uri . '/wp-img/header.jpg"'); ?>');">

        <div class="container" style="">
            <div class="centered text-center " style="left: unset;transform: translate(0%, 0%);width:82%;">
                <h2 class="text-white mb-0 title-tag" style="font-size:60px">Weaving families together,</h2>
                <p class="sub-title-tag text-white offset-5" style="top:-20px !important;font-size:60px">because every
                    child matters</p>


            </div>
            <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
                <span class="ml-3 mr-3 hb-1"></span>
                <span class="ml-3 mr-3 hb-2"></span>
                <span class="ml-3 mr-3 hb-3"></span>
            </div>
        </div>


    </section><!-- #intro -->

    <section id="team" style="padding: 60px 15px !important;">
        <div class="row pt-3">
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
        </div>

        <div class="container" style="max-width: 860px;>
      <div class=" offset
        ">
        <h3 class="mt-2 "><b>Amaris</b></h3>
        <p class="mt-4 font-tofino" style="font-weight: 500;color:#000">Not every child knows the love of family and not
            every family is able to give a child the love they need. We believe every child is worthy of the support,
            encouragement, nurturing, and unconditional love that a permanent family provides. Our heart is to make this
            a reality for every child, teen, and young adult.
        </p>


        <p class=" mt-4 font-tofino" style="font-weight: 500;color:#000">We partner with Albertans like you to weave
            families together from the threads of brokenness and hope so that children can have the opportunity to live
            the life they were created to live.</p>

        </div>


        </div>
    </section>


    <section id="team" style="background: #202020;height: 80vh;width:90%;margin:0 auto;">
        <!---<div class="row pt-3">
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
             </div>

       <div class="container-fluid" style="max-width: 860px;padding-bottom: 50px">
         <div class="offset">
   <h3 class="text-white mt-2 "><b>Who we are.</b></h3>
   <p class="text-white mt-3">Amaris, formerly Christian Adoption Services, was founded in 1989 as a non-profit adoption agency,licensed by the Government of Alberta under the Child, Youth, and Family Enhancement Act to provide professional adoption and family support services to residents of Alberta. With experienced accredited social workers in Calgary, Edmonton, Red Deer, Lethbridge, LaCrete, and Grand Prairie we are able to serve a wide range of communities, both urban and rural, throughout Alberta. Contact us to inquire about your location.</p>

   </div>

   <div class="offset mt-5 pt-4">
   <h3 class="text-white mt-2 "><b>What we do.</b></h3>
   <p class="text-white mt-3">Amaris guides families before, during and after adoption while focusing on meeting the needs of the diverse children and clients we serve. We provide professional, ethical, compassionate, personal, and effective support services for families of all shapes and sizes. We facilitate domestic adoption and assist with international adoption. </p>

   </div>
       </div>-->
    </section>


    <section id="team">
        <div class="container" style="padding: 90px 0px !important;">

            <div class="row justify-content-center">
                <span class="ml-3 mr-3" style="height: 5px;width:20%;background: #8c5776"></span>
                <span class="ml-3 mr-3" style="height: 5px;width:20%;background: #fbb26a"></span>
                <span class="ml-3 mr-3" style="height: 5px;width:20%;background: #f3dd8a"></span>
            </div>
        </div>
    </section>


    <section id="team" class="pt-0">
        <div class="containe " style="padding:15px">


            <div class="row">


                <div class="col-lg-4 col-md-6 wow fadeInUp pr-0 pl-0" data-wow-delay="0.1s">
                    <div class="member main-tile">
                        <img style="height: 280px;width:100%;object-fit: cover"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/c-1.jpg"'); ?>"
                             class="img-fluid" alt="">
                        <div class="centered">
                            <h4 style="text-align:left;font-size:15px;font-weight: bold">Adopting Parents</h4>
                            <p class="mt-4 text-white mb-1" style="text-align:left;font-size:14px;">Considering growing
                                your family through adoption?</p>
                            <a href="<?php echo site_url($path, $scheme); ?>/adopting"
                               class="btn btn-outline-default btn-sm mt-2"
                               style="border-color: #fff !important;color:#fff;border-radius: 1.2rem;float: left;border-width: 2px;">Learn
                                More</a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s">
                    <div class="member main-tile">
                        <img style="height: 280px;width:100%;object-fit: cover"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/c-2.jpg"'); ?>"
                             class="img-fluid" alt="">
                        <div class="centered">
                            <h4 style="text-align:left;font-size:15px;font-weight: bold">Expecting Parents</h4>
                            <p class="mt-4 text-white mb-1" style="text-align:left;font-size:14px;">Considering making
                                an adoption plan for your baby?</p>
                            <a href="<?php echo site_url($path, $scheme); ?>/expecting"
                               class="btn btn-outline-default btn-sm mt-2"
                               style="border-color: #fff !important;color:#fff;border-radius: 1.2rem;float: left;border-width: 2px;">Learn
                                More</a>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6 wow fadeInUp pr-0 pl-0" data-wow-delay="0.3s">
                    <div class="member main-tile">
                        <img style="height: 280px;width:100%;object-fit: cover"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/c-3.jpg"'); ?>"
                             class="img-fluid" alt="">
                        <div class="centered">
                            <h4 style="text-align:left;font-size:15px;font-weight: bold">Family Support</h4>
                            <p class="mt-4 text-white mb-1" style="text-align:left;font-size:14px;">Looking for
                                resources, courses, counseling, or programs to support your family?</p>
                            <a href="<?php echo site_url($path, $scheme); ?>/family-support"
                               class="btn btn-outline-default btn-sm mt-2"
                               style="border-color: #fff !important;color:#fff;border-radius: 1.2rem;float: left;border-width: 2px;">Learn
                                More</a>
                        </div>
                    </div>
                </div>
            </div>


        </div>

        </div>
    </section>


    <section id="testimonials" class="section-bg wow fadeInUp mb-5"
             style="background-color:#202020;background-size:cover;padding-bottom: 90px;">
        <img class="hash-fix" src="<?php echo esc_url($template_directory_uri . '/wp-img/home-main-hash-tag.png"'); ?>">
        <div class="container">

            <header class="section-header">
                <h2 class="offset-3 text-white pl-2"> Not your typical <br> <span style="top:-15px"
                                                                                  class="offset-3 sub-title-tag"
                                                                                  style="">adoption agency</span></h2>
            </header>
            <div class="container" style="max-width: 720px !important;">
                <div class="row">

                    <p class="text-white mt-5">Our love for children and passion to see them experience the beauty of
                        family drive us to great lengths to see as many of them as possible find a permanent home.
                    </p>
                    <p class="text-white mt-2">We’re different because of how much we care and because we’re willing to
                        take on the hard cases...the ones that others pass over...like children with special needs,
                        older kids, and sibling groups. We focus on the child first and put expecting and adopting
                        parents in the driver’s seat, empowering them to lead the process with us as their adoption
                        guide.

                    </p>


                </div>
            </div>
        </div>


        </div>
        <!--<div class="text-right" >
      <img class="hidden-sm" style="width:50%" src="<?php echo esc_url($template_directory_uri . '/wp-img/content-bg-border.png"'); ?>">
    </div>-->
        <div class="row pt-3 justify-content-end" style="position: absolute;margin-top:65px;width:100%">
            <span class="ml-3 mr-3 hb-1" style="height: 15px"></span>
            <span class="ml-3 mr-3 hb-2" style="height: 15px"></span>
            <span class="ml-3 mr-3 hb-3" style="height: 15px"></span>
        </div>
    </section><!-- #testimonials -->


    <!--==========================
      Footer
    ============================-->
<?php include('footer.php'); ?>