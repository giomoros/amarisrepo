<?php /* Template Name: donate */ ?>
<?php include('header.php'); ?>
<body style="overflow-x: hidden;">

  <?php include('navbar.php'); ?>
  <!--==========================
    Intro Section

  ============================-->

  

<section class="header-position" id="team" style="background-image:url('<?php echo esc_url( $template_directory_uri . '/wp-img/donet-header-bg.jpg"' ); ?>');">

    <div class="container" style="">
      <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
        <h3 class="text-white title-tag" style="font-size: 60px;">Donate  <span style="font-size: 60px;" class="sub-title-tag">Give</span></h3>
       
      </div>
    </div>
      <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
         <span class="ml-3 mr-3 hb-1"></span>
         <span class="ml-3 mr-3 hb-2"></span>
         <span class="ml-3 mr-3 hb-3"></span>
          </div>
          
  </section><!-- #intro -->




  <section  id="team" style="background: #202020;padding: 15px;padding-bottom: 20px;width: 90%;">
     <!--<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
       </div>-->

       <div class="container-fluid" style="max-width: 730px;padding: 80px 0px">
        <div class="offset">
          <h3 class="text-white mt-2 ">Make adoption <span class="sub-title-tag">Possible</span></h3>

        </div>
      </div>

<div class="row pt-3">
        
         <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #f3dd8a"></span>
          </div>
         

           <div class="container-fluid" style="padding: 10px 0px">
        <div class="offset-2 mr-5 pl-3">
         

          <p class="text-white mt-5" style="max-width: 94%">
            At Amaris, we rely almost solely on private donations from individuals or businesses who believe that every child deserves to have a familly. Should you choose to give, know that your donation will be distributed in the most fiscally responsible way as we understand that you are trusting us with your giving. Some of the areas that are totally reliant on donations include:
          </p>
<!--<ul class="leaf">
  <li class="li-bullets text-white mt-2 mb-2">Expectant parent or birth parent counseling- we do not charge anything to expectant parents or birth parents who are considering the difficult decision of placing their child for adoption</li>
  <li class="li-bullets text-white  mt-2 mb-2">All of our additional programs, such as our mentor program, respite program, and age out program do not currently have funding</li>
  <li class="li-bullets text-white  mt-2 mb-2">Subsidizing costs for adoptive families as we never want finances to be the reason that a family does not open their home to a child</li>
  <li class="li-bullets text-white  mt-2 mb-2">Additional training for staff to ensure that they are always equipped with the latest understanding and expertise to work with the clients</li>
  <li class="li-bullets text-white  mt-2 mb-21">Collaborative work with the government, other agencies, churches, and the community to ensure that we are doing our best to meet the needs that exist</li>
  <li class="li-bullets text-white  mt-2 mb-2">Getting our name out there so that anyone who requires our services is aware of who we are and the work we do
</li>
</ul>-->




<ul class="list-unstyled">
  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">Expectant parent or birth parent counseling- we do not charge anything to expectant parents or birth parents who are considering the difficult decision of placing their child for adoption</p>
    </div>
  </li>

  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">All of our additional programs, such as our mentor program, respite program, and age out program do not currently have funding</p>
    </div>
  </li>

  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">Subsidizing costs for adoptive families as we never want finances to be the reason that a family does not open their home to a child</p>
    </div>
  </li>

  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">Additional training for staff to ensure that they are always equipped with the latest understanding and expertise to work with the clients</p>
    </div>
  </li>

  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">Collaborative work with the government, other agencies, churches, and the community to ensure that we are doing our best to meet the needs that exist</p>
    </div>
  </li>

  <li class="media mb-2">
    <div class="dashed"></div>
    <div class="media-body">
      <p class="mt-0 mb-1 text-white">Getting our name out there so that anyone who requires our services is aware of who we are and the work we do</p>
    </div>
  </li>
 
</ul>





        </div>
      </div>

    </section>



 


    
  
  






<section style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url( $template_directory_uri . '/wp-img/donet-family.jpg"' ); ?>')" id="team">

  <div class="row justify-content-end full-width" style="position:relative;top:18%;left:0%;height:100%;width:100%">
  <img style="width:35%;z-index: 1;position:absolute;top:44%;right:-15px;" src="<?php echo esc_url( $template_directory_uri . '/wp-img/tag.png"' ); ?>">

</div>

</section>





<section id="team" style="padding: 90px 0;background: #202020;height:40%;width: 90%; margin: 0 0 0 auto;position: relative;top:-80px">
    

    <div class="row pt-3 justify-content-start" style="position: absolute;bottom: -5px;width:100%;left:0px">
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #f3dd8a"></span>
          </div>
  

  </section>



  <!--==========================
    Footer
  ============================-->
  <?php include('footer.php'); ?>