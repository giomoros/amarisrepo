<?php /* Template Name: expecting */ ?>
<?php include('header.php'); ?>
<body style="overflow-x: hidden;">

 <?php include('navbar.php'); ?>

  <!--==========================
    Intro Section

    ============================-->

    

    <section class="header-position" id="team" style="background-image:url('<?php echo esc_url( $template_directory_uri . '/wp-img/baby-header.jpg"' ); ?>');">

      <div class="container" style="">
        <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
         <h3 class="text-white title-tag" style="font-size: 60px;">Expecting <span class="text-white sub-title-tag" style="font-size: 60px;">Parents</span></h3>


       </div>
     </div>
     <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
       <span class="ml-3 mr-3 hb-1"></span>
       <span class="ml-3 mr-3 hb-2"></span>
       <span class="ml-3 mr-3 hb-3"></span>
     </div>
     
   </section><!-- #intro -->




   
   
   

   <section id="team" style="padding: 60px 15px !important;background-color: #202020;width: 95%;">
    <div class="container">

     <h3 class="text-white pt-5 mb-0 title-tag" style="font-size: 35px;">The calling of a parent is to love your child and do everything <br>you can for their well being </h3>
     <p class="sub-title-tag text-white offset-5" style="top:-35px !important;line-height: 0.7;font-size: 78px;">and sometimes that thing is <br> to entrust someone else to raise them.</p>

   </div>
   <div class="row pt-3" >
     <span class=" mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
     <span class=" mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
     <span class=" mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
   </div>

   <div class="container" style="max-width: 900px;">
    <div class="offset">
      <p class="mt-5 text-white ml-3">Life doesn’t always go according to plan, we understand that. The idea of having a child before you’re ready is daunting. But what’s beautiful is that in the midst of all this, there is possibility and hope for this child’s life. You might be scared or confused, trying to navigate next steps. We get it, we understand, and we’re here for you. Whatever has been said up until now, we’re saying what’s true: you are brave for thinking of this child.  </p>

      <p class="text-white ml-3">Choosing to place your child into a family where the life you envision for them is possible - it’s hard and brave and courageous and loving. We’re here to walk with you every step of the way. Through the pregnancy, during the birth, helping choose adoptive parents, placing your child in their care, and navigating life afterwards. As long as you need us, we’re here to guide you, answer your questions, and support you in whatever way we can to help you make the best possible choice for you and your child.  </p>

    </div>


  </div>
</section>


<section class="mt-5" style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url( $template_directory_uri . '/wp-img/expecting-lady.jpg"' ); ?>')" id="team">

  <div class="row justify-content-center" style="position: relative;top:30%">

   <h3 class="text-white pt-5 mb-0 title-tag">They (the adoptive family) invited me to see my baby;</h3><br>
   <p class="sub-title-tag text-white offset-3" style="top:-20px !important;">it was the greatest gift anyone could have ever given me</p>
 </div>
 <div class="row justify-content-end full-width" style="position:relative;top:0%;left:0%;height:100%;width:100%">
  <img style="width:50%;z-index: 1;position:absolute;top:5%;right:-15px;" src="<?php echo esc_url( $template_directory_uri . '/wp-img/tag.png"' ); ?>">

</div>

</section>



<section id="team"  style="background: #202020;padding: 15px;padding-bottom: 60px;width: 90%; margin: 0 0 0 auto;position: relative;top:-100px">
     <!--<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
       </div>-->

       <div class="container-fluid ml-0" style="max-width: 1000px;padding: 50px 60px">
        <div class="offset">
          <h3 class="text-white mt-2 "><b>Child-first-Client/parent-led</b></h3>
          <p class="text-white mt-2">We are 100% committed to you and your child and will always make your child’s well being our top priority. We are a “child-first” & “parent-led” adoption agency which means…
          </p>

          <p class="text-white mt-2 ml-5">Your child’s needs and well being always come first. We never compromise on this and have careful procedures and policies in place to protect the child at every stage. </p>
          <p class="text-white ml-5">
            We put you in the driver's seat, allowing you to decide how fast you want to move and how you want things to work. We’ll never pressure you or ask you to do anything you’re not comfortable with. You are in charge. Not us. Not the adoptive parents. You. We work on your behalf and empower you with the tools, resources, and knowledge to make the best choices possible. 
          </p>

        </div>

      </div>
    </section>



    
    <section id="team" class="pt-0" style="padding: 0px 0 60px !important;">
     <div class="row pt-3" >
       <span class="mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
       <span class="mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
       <span class="mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
     </div>

     <div class="container">
      <div class="offset-1 p-3">
        <h4><b>How it works.</b></h4>
        <div class="row mt-5 mb-5">
          <div class="col-md-3 pl-0">
            <div class="box pl-3 pt-3 pb-3 fw-500 text-white" style="background-color:#8c5776;max-width:90%;font-size: 24px;">Process</div>
          </div>
        </div>


        <div class="row m-0">

         <div class="row pt-2" style="width:100%;">
           <span class="" style="height: 15px;width:100%;background: #8c5776"></span>

           <p class="pr-5 mt-2 text-dark fw-500">Once you get in touch with us, we will connect you with one of our birth parent counselors from across the province, depending on your location. They will walk with you through your hopes and fears and will go through your options with you. Once you are at a place where you feel comfortable moving forward, if you choose adoption for your child, you will then be shown profiles of fully screened and trained adoptive families for you to consider. 
           </p>

           <p class="pr-5 mt-2 text-dark fw-500">If you are able to narrow it down to one or two families, a meeting time will be scheduled for you to meet the family, ask any questions, voice any concerns, and see how you feel about them. Depending on how far along in the pregnancy you are (or if the child is already born), subsequent visits are scheduled to make sure that everyone feels it is a good fit and there is clarity around openness, etc. Prior to the birth, you also have an opportunity to make a hospital or birthing plan, including choosing who you would like to be present and how we can best support you through the process. Once you sign consent for the adoption, the child goes into the care of the adoptive family. There is a 10 day revocation period where you are able to carefully consider your choice. You have the option of changing your mind at any time during that period. After that time , we continue to be available to you for follow up support and assisting in ongoing visits and contact as needed.</p>
         </div>
       </div>


       

<!--<div class="row mt-5 mb-4">
  <div class="col-md-3 pl-0">
    <div class="box pl-2 pt-2 pb-2 text-white" style="background-color:#fbb26a;max-width:90%; ">Timelines</div>
  </div>
</div>

<div class="row">
  <p class="w-75">Christian Adoption Services does not receive any government funding. The costs of our programs are covered by fees charged to adoptive families, fund-raising and donations. All services to families considering an adoption plan for their child are free.</p>
  </div>

  <div class="row mt-5 mb-4">
  <div class="col-md-3 pl-0">
    <div class="box pl-2 pt-2 pb-2 text-white" style="background-color:#f3dd8a;max-width:90%; ">Cost</div>
  </div>
</div>

<div class="row">
  <p class="w-75">please see our domestic fee schedule for a breakdown of costs. Most families end up spending around $15,000 to complete a domestic adoption</p>
</div>-->


</div>
</div>
</div>
</section>




<section class="mt-5" style="height: 100vh;background-size:cover;background-image: url('http://localhost/amaris/wp-content/themes/startwordpress/wp-img/baby-walk.jpg')" id="team">

  <div class="row justify-content-start" style="position: relative;top:40%;left:10%">


    <h3 class="text-white pt-5 mb-0 title-tag">I have no regrets and I thank God that</h3><br>
    <p class="sub-title-tag text-white offset-2" style="top:-20px !important;">He led me to these wonderful, caring people</p>
  </div>
  <div class="row justify-content-end full-width" style="position:relative;top:0%;left:0%;height:100%;width:100%">
    <img style="width:50%;z-index: 1;position:absolute;top:5%;right:-15px;" src="http://localhost/amaris/wp-content/themes/startwordpress/wp-img/tag.png">

  </div>

</section>


<section id="team" style="background: #202020;padding: 15px;padding-bottom: 60px;width: 90%; margin: 0 0 0 auto;position: relative;top:-100px">
     <!--<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
       </div>-->

       <div class="container-fluid ml-0" style="max-width: 1000px;padding: 50px 60px">
        <div class="offset">
          <h3 class="text-white mt-2 "><b>Why open adoption?</b></h3>
          <p class="text-white mt-2">The majority of birth families we meet with have a desire to know how their child is doing after placement, and even have an ongoing relationship with them. Through both our own experience and recent research we know this is best for the child. As they grow it is critical to discovering who they are, their adoption story, and for the many people around them (both biological and adopted) that love them and want the best for them. As a result, unless otherwise indicated by the birth family, all domestic adoptions through our agency are open adoptions, where there is ongoing visits and contact between the biological family and the adoptive family. 
          </p>

          <p class="text-white mt-2 ">This is something that is pre-discussed and agreed upon prior to the placement of the child. It is based on the wishes of the birth parent(s) as well as other more logistical pieces, such as distance and scheduling. All of our domestic families are carefully screened, trained, and supported around the importance of open adoption and are not accepted into our program if they do not feel they can follow through in this area.</p>


        </div>

      </div>
    </section>



    <section id="team" style="padding: 60px 15px !important;">
     <div class="row pt-3">
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
     </div>

     <div class="container" style="max-width: 860px;">
      <div class="offset">
        <h3 class="mt-4" style="color: #000"><b>Our promise.</b></h3>
        <p class=" mt-4 mb-3 fw-500" style="color: #000">Our desire is that every person that comes to us for support feels respected, empowered, supported, and never judged. Because of this, we make the following promises to every expectant parent or birth parent(s) that we come into contact with:</p>

        <p class="mb-3 ml-5" style="color: #000"><b>We will always respect and honor you as the parent of your child with their best interests in mind</b></p>
        <p class="mb-3  ml-5" style="color: #000"><b>We will always be transparent and honest with our information, always presenting every option and supporting you regardless of what you choose </b></p>
        <p class="mb-3  ml-5" style="color: #000"><b>We promise to be collaborative and work with whoever you see to be a supportive or helpful resource in your life</b></p>
        <p class="mb-3  ml-5" style="color: #000"><b>We promise to be inclusive and loving, always operating without judgement and never shaming or secret-keeping</b></p>
        <p class="mb-3  ml-5" style="color: #000"><b>We promise to always work tirelessly and passionately to ensure that every child placed through our agency has the best possible family to grow up in</b></p>


      </div>
    </section>

    <section id="team" style="padding: 60px 15px !important;">
     <div class="row pt-3">
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
     </div>

     <div class="container" style="max-width: 860px;">
      <div class="offset">
        <h3 class="mt-4" style="color: #000"><b>Adoption options.</b></h3>
        <p class=" mt-4 mb-3 fw-500" style="color:#000">As you face an unplanned pregnancy, you may be unsure as to what your options are. This can be a very scary and overwhelming time, but we would love to come alongside you to support you, give you more information about your options, and guide you through whatever decision you decide to make. If adoption is not the right fit for you, we are committed to connecting you with resources and support to ensure that you receive what you need to feel that you can parent successfully.</p>


      </div>
    </section>


    <section id="team" style="padding: 60px 15px !important;">
     <div class="row pt-3">
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
     </div>

     <div class="container" style="max-width: 860px;">
      <div class="offset">
        <h3 class="mt-4" style="color: #000"><b>How we care of you.</b></h3>
        <p class=" mt-4 mb-3 fw-500 " style="color:#000">Choosing adoption for your child is an incredibly selfless and loving choice as it involves a decision to experience heartache and loss in order to provide your child with the life you envision for them. We understand how difficult this journey can be and only desire to come alongside you, care for you, empower you with information, and support whatever decision you make. We want to arm you with enough information to feel like you can make the best possible decision for yourself and your baby, and then walk with you through that without judgment, and with honesty and transparency.</p>


        <div class="row" id="team">
          <div class="col-md-4">
           <div class="row pt-3 justify-content-end" style="width:100%;">
             <span class="ml-3 mr-3 pl-3 pt-1 text-white pb-1" style="width:100%;background: #8c5776;font-size:18px;"><b style="font-family: tofino">Before</b></span>
           </div>
           
           <p style="max-width: 80%;margin: 0px 15px auto;font-size: 14px;" class="text-dark fw-500">Your initial contact with our staff will be to book an adoption options sessions with one of our birth parent counselors. They will explore your options with you in an open and non-judgmental way. They will allow you to take whatever time you need to come to the best possible decision for you, and will support you along the way.</p>
           
         </div>

         <div class="col-md-4">
           <div class="row pt-3 justify-content-end" style="width:100%;">
            <span class="ml-3 mr-3 pl-3 pt-1 text-white pb-1" style="width:100%;background: #fbb26a;font-size:18px;"><b style="font-family: tofino">During</b></span>
          </div>
          
          <p style="max-width: 80%;margin: 0px 15px auto;font-size: 14px;" class="text-dark fw-500">If you choose to move forward with an adoption plan, you will be shown profiles of screened and trained adoptive families that fit your criteria. If you are not excited about the families you are shown, we will continue to walk with you until you feel comfortable with one of them.
          </p>
          <p style="max-width: 80%;margin: 0px 15px auto;font-size: 14px;" class="text-dark fw-500 mt-4">Once you have chosen your family, our social worker will arrange and facilitate a meeting with you and the family. As with every part of the process, we aim for this to happen on a timeline that is comfortable for you. After as many visits as you need, if you decide to move forward, a birth plan will be completed that indicates your desires. You may choose to have the adoptive family present for the birth, or may desire more time and space to process on your own or with your support system. Many birth parents choose to write a letter to their child at this time or have a ceremony to honor the choice they are making. Whatever you choose, know that with respect and care, we are happy to be with you every step of the way.
          </p>

          
        </div>

        <div class="col-md-4">
         <div class="row pt-3 justify-content-end" style="width:100%;">
           <span class="ml-3 mr-3 pl-3 pt-1 text-white pb-1" style="width:100%;background: #f3dd8a;font-size:18px;"><b style="font-family: tofino">After</b></span>
         </div>
         
         <p style="max-width: 80%;margin: 0px 15px auto;font-size: 14px;" class="text-dark fw-500">The adjustment period after the placement of a child can be a very emotional and difficult time. Although you may believe you have made the right decision for your child, there is very real grief and loss that usually occurs once the child has been placed. We are committed to continuing to walk with you through this difficult time and ensuring that you have the right support and resources you need. Know that you are allowed to be both happy and sad at the same time and there is no set time limit for when you are expected to ‘feel better’. 
          This is also the time where you will begin to have emails or texts with pictures and updates, as well as get into a rhythm with the family around in person visits and ongoing contact. Although this looks different for every family, we are committed to continuing to work with everyone to ensure the best interests of all involved.
        </p>
        
      </div>

    </div>


  </div>
</section>




<section id="team" style="background: #202020;padding: 15px;padding-bottom: 60px;">


  <div class="container">

   <h3 class="text-white pt-5 mb-0 title-tag text-right">I am happy with my decision, but also sad at times,</h3>
   <p class="sub-title-tag text-white offset-2 text-right" style="top:10px !important;line-height: 0.6">but I still see her and I feel better knowing how<br> much she is loved</p>
 </div>
 <div class="row pt-3">
   <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
   <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
   <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
 </div>



 <div class="container-fluid" style="max-width: 800px;padding-bottom: 50px">
  <div class="offset">
    <h3 class="text-white mt-2 "><b>Have questions?</b></h3>
    <p class="text-white mt-3">You’ve probably got a million questions...we’re here to answer them all. Here’s a good place to start or text us at (403) 444-5555, pop your question in the chat, or book a free consultation call to talk to someone directly.</p>


    <div class="groups">
      <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapseTwo"style="cursor: pointer;">How do I know if I’m ready to adopt?  
        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
        <p id="collapse1" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
        <span class="progress mt-2 ml-5 prog-align" ></span> 
      </div>

      <div class="groups">
        <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How do I know which option is right for me?  
          <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
          <p id="collapse2" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
          <span class="progress mt-2 ml-5 prog-align" style="left:44%"></span> 
        </div>

        <div class="groups">
          <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Where are you guys located?  
            <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
            <p id="collapse3" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
            <span class="progress mt-2 ml-5 prog-align" style="left:29%"></span> 
          </div>

          <div class="groups">
            <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Where do you have social workers?  
              <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
              <p id="collapse4" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
              <span class="progress mt-2 ml-5 prog-align" style="left:35%"></span> 
            </div>



            <div class="groups">
              <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">What are the qualifications to adopt? 
                <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                <p id="collapse5" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                <span class="progress mt-2 ml-5 prog-align"></span> 
              </div>


              <div class="groups">
                <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">What is the home study  and what is the assessment process like?
                  <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                  <p id="collapse6" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                  <span class="progress mt-2 ml-5 prog-align"style="left:64%"></span> 
                </div>

                
                <div class="groups">
                  <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How much does it cost to adopt?
                    <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse7" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align"style="left:33%"></span> 
                  </div>
                  

                  
                  <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How long does it take to adopt?
                      <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                      <p id="collapse8" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                      <span class="progress mt-2 ml-5 prog-align" style="left:32%"></span> 
                    </div>

                    <div class="groups">
                      <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Is there any financial assistance available for adoptive families?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                        <p id="collapse9" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                        <span class="progress mt-2 ml-5 prog-align" style="left:62%"></span> 
                      </div>



                      <div class="groups">
                        <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Are there age restrictions to adopt?
                          <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                          <p id="collapse10" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                          <span class="progress mt-2 ml-5 prog-align"style="left:36%"></span> 
                        </div>

                        <div class="groups">
                          <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Can I adopt as a single?
                            <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                            <p id="collapse11" aria-labelledby="headingTwo" data-parent="#accordionExample" class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest questions.it's all here, just have to keep reading on and on.</p>
                            <span class="progress mt-2 ml-5 prog-align"style="left:24%"></span> 
                          </div>


                          <div class="offset mt-5">
                            <p class="text-white mt-2 mb-2"><b>Download FAQ pdf</b></p>
                            <div class="">
                              <div class="progress" style="width: 100px;height: 5px;border-radius: 20px"></div>
                              <div class="progress ml-3 mt-1" style="width: 100px;height: 5px;border-radius: 20px"></div>
                            </div>
                          </div>
                        </div>


                      </div>
                    </section>


                    <section id="team" style="padding: 60px 15px !important;">
                     <div class="row pt-3">
                       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
                       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
                       <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
                     </div>

                     <div class="container" style="max-width: 860px;">
                      <div class="offset">
                        <h3 class="mt-4" style="color:#000"><b>Resource Centre</b></h3>
                        <div class="row mb-5 text-center" style="padding: 0px 20px;">
                          <a href="<?php echo site_url( $path, $scheme ); ?>/donate" class="btn btn-outline-default btn-sm mt-2" style="border-color: #666 !important;color:#666;border-radius: 1.2rem;width:120px;border-width: 3px;"><b>Learn More</b></a> 
                        </div>
                      </div>
                    </div>
                  </section>


  <!--==========================
    Footer
    ============================-->
    <?php include('footer.php'); ?>