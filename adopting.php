<?php /* Template Name: adopting */ ?>
<?php include('header.php'); ?>

    <body style="overflow-x: hidden;">

<?php include('navbar.php'); ?>
    <!--==========================
      Intro Section

    ============================-->


    <section class="header-position" id="team"
             style="background-image:url('<?php echo esc_url($template_directory_uri . '/wp-img/adopting-header.png"'); ?>');">

        <div class="container" style="">
            <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
                <h3 class="text-white title-tag" style="font-size: 60px;">Adopting <span style="font-size: 60px;"
                                                                                         class="text-white sub-title-tag ">Parents</span>
                </h3>

            </div>
        </div>
        <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
            <span class="ml-3 mr-3 hb-1"></span>
            <span class="ml-3 mr-3 hb-2"></span>
            <span class="ml-3 mr-3 hb-3"></span>
        </div>

    </section><!-- #intro -->


    <section id="team" style="padding: 60px 15px !important;background-color: #202020;width: 95%;">
        <div class="container pt-5">
            <h4 class="mt-4 mb-0 text-white title-tag text-right">Adoption is not about finding children for families,
            </h4>
            <p class="sub-title-tag text-right text-white" style="top:-20px">it's about finding families for
                children. </p>
        </div>
        <div class="row pt-3">
            <span class=" mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class=" mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class=" mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
        </div>

        <div class="container" style="max-width: 900px;">
            <div class="offset">
                <p class="mt-5 text-white">However you have arrived at adoption, whether infertility, a sense of
                    calling, a family circumstance, a heart for children, or any other reason we know it’s not an easy
                    decision. We understand the emotional challenges that come with adoption and how the process can
                    take its toll on relationships. We take your family’s adoption journey very seriously and walk
                    alongside you every step of the way to make sure you have everything you need to make the process as
                    seamless as possible. </p>

                <p class="mt-5 text-white">Our passionate team are not just adoption specialists because of our training
                    or because we have walked alongside many families like yours as they have welcomed a new child into
                    their family, but also because all of us have been through the process ourselves. We understand the
                    ups and downs and joys and difficulties that come with embracing a new child as your own. We pour
                    all of our knowledge, passion, and experience into guiding you through your adoption journey.</p>

            </div>


        </div>
    </section>


    <section class="mt-5"
             style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url($template_directory_uri . '/wp-img/sunshine.jpg"'); ?>')"
             id="team">

        <div class="row justify-content-center" style="position: relative;top:40%">
            <h3 class="text-white   mb-0 title-tag">Would highly recommend your agency... </h3>
            <p class="sub-title-tag text-white offset-5" style="top:-20px !important"> very professional along with care
                and respect</p>
        </div>
        <div class="row justify-content-end full-width" style="position:relative;top:0%;left:0%;height:100%;width:100%">
            <img style="width:50%;z-index: 1;position:absolute;top:26%;right:-15px;"
                 src="<?php echo esc_url($template_directory_uri . '/wp-img/tag.png"'); ?>">

        </div>

    </section>


    <section id="team"
             style="background: #202020;min-height:40%;width: 90%; margin: 0 0 0 auto;position: relative;top:-80px;">

        <div class="container pb-1" style="padding: 0px 20px ;">
            <h3 class="text-white"><b>What you need to know.</b></h3>
            <p class="text-white" style="max-width: 880px;">Adoption legislation is regulated by each individual
                province. In Alberta, both private agencies and Alberta Adoptions work together to ensure a safe and
                ethical adoption process is followed. Amaris works tirelessly to not only find a family for every child,
                but also ensure that the utmost professionalism and ethics are adhered to. Adoptions are both a
                wonderful and difficult thing for everyone involved as it includes both loss and the welcoming of a new
                member into a family. Because of its complexity, we know that adoption is not for everyone, but we
                believe that everyone can contribute in a meaningful way to ensure that every child, youth and young
                adult has ‘people’ to call their own.</p>


            <div class="row pt-3">
                <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #8c5776"></span>
                <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #fbb26a"></span>
                <span class="ml-3 mr-3" style="height: 5px;width:23.6666%;background: #f3dd8a"></span>
            </div>


            <h3 class="text-white mt-5"><b>Why open adoption?</b></h3>
            <p class="text-white" style="max-width: 880px;">More and more in both domestic and international adoptions,
                the research and trend has been towards open, versus closed, adoptions. This means that there is some
                kind of ongoing contact between the birth family and the adoptive family over the life of the child.
                Research very clearly shows that open adoption facilitates healthier and more empowered birth families
                as well as adoptive children who grow up to be more well-adjusted adults. As a result of this, Amaris
                tries to support an open adoption process wherever possible and train and educate our families
                accordingly.</p>


        </div>
    </section>


    <section>
        <div class="container">
            <h3 class="ml-3"><b>Four Adoption Options</b></h3>
            <div class="row">

                <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s"
                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                    <a href="#International-Adoption">
                        <div class="member" style="cursor: pointer;">
                            <img style="width:100%;object-fit: cover"
                                 src="<?php echo esc_url($template_directory_uri . '/wp-img/divi-1.png"'); ?>"
                                 class="img-fluid" alt="">
                            <div class="centered" style="left:46%;top:30%">
                                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">
                                    International Adoption</p>
                            </div>
                        </div>
                    </a>
                    <p class="mt-3 w-75" style="margin:0 auto"> The process of adopting a child from another country and
                        bringing them to live with you in your home country.</p>
                </div>

                <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s"
                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                    <a href="#Domestic-Adoption">
                        <div class="member">
                            <img style="width:100%;object-fit: cover"
                                 src="<?php echo esc_url($template_directory_uri . '/wp-img/divi-2.png"'); ?>"
                                 class="img-fluid" alt="">
                            <div class="centered" style="left:46%;top:30%">
                                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Domestic
                                    Adoption</p>
                            </div>
                        </div>
                    </a>
                    <p class="mt-3 w-75" style="margin:0 auto">Adopting a child from your home province. In domestic
                        adoption, birth families choose the family they want to place their child in.</p>
                </div>

                <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s"
                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                    <a href="#Government-Adoption">
                        <div class="member">
                            <img style="width:100%;object-fit: cover"
                                 src="<?php echo esc_url($template_directory_uri . '/wp-img/divi-3.png"'); ?>"
                                 class="img-fluid" alt="">
                            <div class="centered" style="left:46%;top:30%">
                                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Government
                                    Adoption</p>
                            </div>
                        </div>
                    </a>
                    <p class="mt-3 w-75" style="margin:0 auto">The adoption of a child who is in the care and custody of
                        Alberta Child and Family Services.</p>
                </div>

                <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s"
                     style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                    <a href="#Private-Direct">
                        <div class="member">
                            <img style="width:100%;object-fit: cover"
                                 src="<?php echo esc_url($template_directory_uri . '/wp-img/divi-4.png"'); ?>"
                                 class="img-fluid" alt="">
                            <div class="centered" style="left:46%;top:30%">
                                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Private
                                    Direct</p>
                            </div>
                        </div>
                    </a>
                    <p class="mt-3 w-75" style="margin:0 auto">A biological family in Alberta has the right to place
                        their child directly into another family’s home for adoption</p>
                </div>


            </div>
        </div>
    </section>


    <section id="team" style="padding: 60px 15px !important;">
        <div class="row pt-3">
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
        </div>

        <div class="container" style="max-width: 860px;">
            <div class="offset">
                <h3 class="mt-4"><b>Making the best choice</b></h3>
                <p class=" mt-4 mb-3">When considering adoption, there are many factors to take into account. Many
                    people have a very clear idea of the type or age of child(ren) that they would best be able to care
                    for. As the focus is not on “finding a child for a family”, but rather “finding a family for a
                    child”, it is important to ask yourself hard questions around what you feel you would, or would not,
                    be able to effectively manage. If you are a couple dealing with infertility and have a strong desire
                    for a newborn child, your only options would be either domestic or the United States. If you are not
                    open to any special needs (which includes older children or sibling groups) then you would not be
                    suitable for a government adoption. It is important that you feel a strong leading at some point as
                    you will need to fully welcome and embrace the child’s culture, language, special needs, and unique
                    traits into your home.</p>


            </div>
        </div>
    </section>


    <section id="team" style="padding: 60px 15px !important;">
        <div class="row pt-3">
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
        </div>

        <div id="International-Adoption" class="container pt-5">
            <div class="offset">
                <div class="row pt-5">
                    <div class="col-md-2 p-3"
                         style="background: #8c5776;padding-right: 0px !important;margin-bottom:120px;">
                        <h5 class="text-white">International Adoption</h5>
                        <img class="w-100"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/glob-tag.png"'); ?>">
                    </div>
                    <div class="col-md-10">
                        <div class="offset-1 mb-5 ">
                            <h4><b>Overview</b></h4>
                            <p>We believe that international adoption should be seen as the last option for that child.
                                Although it is a wonderful gift to the family and the child, it is both a complex and
                                difficult process where the child is removed from everything they know and much of what
                                makes them who they are. Due to certain circumstances, however, there are cases where
                                these children would have little to no chance of a healthy life aside from being adopted
                                internationally. In these cases, Amaris gladly walks with you throughout the journey to
                                guide you along the way.</p>
                        </div>

                        <div class="offset-1 mb-5 mt-5">
                            <h4><b>Timeline</b></h4>
                            <p>Dependent on which country you choose, but average of around 3 years from start to
                                placement</p>
                        </div>

                        <div class="offset-1 mb-5 mt-5">
                            <h4><b>Steps</b></h4>

                            <div class="row m-0 pl-4">
                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">1<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">choose</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Decide which country you
                                            wish to adopt from and select coordinating agency.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">2<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">apply</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Fill out all paperwork,
                                            apply for police & child intervention checks, complete medical report.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">3<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;"> prepare</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Schedule home study,
                                            sign up for training, review home study & send to Alberta Adoptions, start
                                            citizenship application.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">4<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">learn</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Compile dossier & send
                                            to coordinating agency, lear as much as possible about adoption while
                                            waiting for referral. </p>
                                    </div>
                                </div>

                            </div>


                            <div class="row m-0 pl-4">
                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">5<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">referral</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Review referral, get
                                            medical consult, decide to move forward and adopt or not.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">6<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">accept</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Formally accept
                                            referral, contact coordinating agency for next steps and travel plans.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">7<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">travel</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Travel to country of
                                            origin to pick up child. </p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 " style="color:#8c5776;font-size: 60px;"><b
                                                    style="font-family:tofino;">8<span class="ml-2"
                                                                                       style="color:#8c5776;font-size: 25px;vertical-align: middle;"><b
                                                            style="font-family:tofino;">finalize</b></span></b></h1>
                                        <span class="" style="height: 15px;width:100%;background: #8c5776"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Arrive at home and book
                                            post placement reports with coordinating agency. Contact Amaris for any
                                            support you need.</p>
                                    </div>
                                </div>

                            </div>


                        </div>


                        <div class="offset-1 mb-5">
                            <h4><b>Cost</b></h4>
                            <p>Please see our fee schedule for costs associated with our agency. Regardless of what
                                country you choose to adopt from, our fees remain the same. The differing costs depend
                                on each country…most families report averages from $30,000 to $75,000 </p>
                        </div>

                        <div class="offset-1 mb-5">
                            <h4><b>Training Required</b></h4>
                            <p>You are required to complete a minimum of one full day training. All individuals wanting
                                to adopt must attend. These trainings are offered once per month alternating between
                                Calgary and Edmonton. We also highly recommend doing a lot of reading and research,
                                specifically in the areas of attachment, grief and loss, trauma, transracial adoption
                                issues, and bonding strategies. Please see our resource section for more recommended
                                reading and resources. </p>
                        </div>

                        <div class="offset-1 mb-5">
                            <div class="row mb-5 text-left">
                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:100px;border-width: 3px;">Stories</a>

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:80px;border-width: 3px;">Apply</a>

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:170px;border-width: 3px;">Book
                                    A Consulation</a>
                            </div>

                            <div class="row pt-3">
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #8c5776"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #8c5776"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #8c5776"></span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div id="Domestic-Adoption" class="container pt-5">
            <div class="offset">
                <div class="row pt-5">
                    <div class="col-md-2 p-3"
                         style="background: #fbb26a;padding-right: 0px !important;margin-bottom:120px;">
                        <h5 class="text-white">Domestic Adoption</h5>
                        <img class="w-100"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/dom-tag.png"'); ?>">
                    </div>
                    <div class="col-md-10">
                        <div class="offset-1 mb-5 ">
                            <h4><b>Overview</b></h4>
                            <p>Domestic adoptions through a private agency like Amaris generally involves an expectant
                                parent contacting the agency to make an adoption plan for their child. They are then
                                able to choose from profiles of screened and trained adoptive families and are supported
                                and empowered through the process of placement. These adoptions are all considered ‘open
                                adoptions’ where contact is maintained with the birth family as the child grows. There
                                are also occasional circumstances where an older child may be placed through our
                                domestic program, or a child who may be receiving services from the government may be
                                placed with one of our families rather than going into foster care.</p>
                        </div>


                        <div class="offset-1 mb-5 mt-5">
                            <h4><b>Steps</b></h4>

                            <div class="row m-0 pl-4">
                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>1<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">interview</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Contact us to schedule
                                            preliminary interview to determine fit, add your name to waitlist. </p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>2<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">application</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Once you are close to
                                            top of waitlist, start application paperwork including police checks and
                                            medical assessment.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <div class="media">
                                            <div class="media-left">
                                                <h4 class="media-heading mb-0"
                                                    style="color:#fbb26a;font-size: 60px;font-weight: 600">3</h4>
                                            </div>
                                            <div class="media-body">
                                                <h4 style="color:#fbb26a;font-size: 25px;font-weight: 600"
                                                    class="media-heading mb-0 mt-1  ml-2">home<br> study</h4>

                                            </div>
                                        </div>
                                        <!--<h1 class="mb-0 " style="color:#fbb26a;font-size: 60px;"><b>3<span class="ml-2" style="color:#fbb26a;font-size: 25px;vertical-align: middle;">home study</span></b></h1>-->
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2"> Schedule home study and
                                            book domestic adoption training sessions.
                                        </p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>4<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">approval</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Wait for approval from
                                            Domestic Adoption Program, at which point you become active.
                                        </p>
                                    </div>
                                </div>

                            </div>


                            <div class="row m-0 pl-4">
                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <div class="media">
                                            <div class="media-left">
                                                <h4 class="media-heading mb-0"
                                                    style="color:#fbb26a;font-size: 60px;font-weight: 600">5</h4>
                                            </div>
                                            <div class="media-body">
                                                <h4 style="color:#fbb26a;font-size: 25px;font-weight: 600"
                                                    class="media-heading mb-0 mt-1  ml-2">profile <br> creation</h4>

                                            </div>
                                        </div>
                                        <!--<h1 class="mb-0 " style="color:#fbb26a;font-size: 60px;"><b>5<span class="ml-2" style="color:#fbb26a;font-size: 25px;vertical-align: middle;">referral</span></b></h1>-->
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Create adoption profile
                                            to be presented to potential birth families, learn as much as you can about
                                            adoption while you wait for a match.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>6<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">match</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Once a birth family
                                            selects your profile we will contact you to set up first meeting.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>7<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">meet</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">Several meetings with
                                            birth parents will follow. Birth parents lead the process based on comfort
                                            level.</p>
                                    </div>
                                </div>


                                <div class="col-md-3 pl-0 pr-0">
                                    <div class="row pt-3" style="width:100%;">
                                        <h1 class="mb-0 mt-0" style="color:#fbb26a;font-size: 60px;"><b>8<span
                                                        class="ml-2"
                                                        style="color:#fbb26a;font-size: 25px;vertical-align: middle;">placement</span></b>
                                        </h1>
                                        <span class="" style="height: 15px;width:100%;background: #fbb26a"></span>
                                        <p style="max-width: 90%;font-size: 14px;" class="mt-2">If both families agree
                                            to proceed the child will be placed in your care. After 10 day revocation
                                            period passes, placement becomes final and adoption documents are
                                            compiled.</p>
                                    </div>
                                </div>

                            </div>


                        </div>

                        <div class="offset-1 mb-5">
                            <div class="row mb-5 text-left">
                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:100px;border-width: 3px;">Stories</a>

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:80px;border-width: 3px;">Apply</a>

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:170px;border-width: 3px;">Book
                                    A Consulation</a>
                            </div>

                            <div class="row pt-3">
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #fbb26a"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #fbb26a"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #fbb26a"></span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div id="Government-Adoption" class="container pt-5">
            <div class="offset">
                <div class="row pt-5">
                    <div class="col-md-2 p-3"
                         style="background: #f3dd8a;padding-right: 0px !important;margin-bottom:120px;">
                        <h5 class="text-white">Government Adoption</h5>
                        <img class="w-100"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/gov-tag.png"'); ?>">
                    </div>
                    <div class="col-md-10">
                        <div class="offset-1 mb-5 ">
                            <h4><b>Overview</b></h4>
                            <p>Government adoptions involve the adoption of a child who has been in the care of Child
                                and Family Services. These children were apprehended from their biological families for
                                a number of reasons due to their health and safety being at risk. There are currently
                                many children in our province’s care looking for forever families and these adoptions do
                                not come with any costs to the adoptive family. *please note that we are not generally
                                involved in government adoptions as we are a private agency. That being said, because we
                                are passionate about every child finding a forever home, we feel that it is important to
                                list this as an option for you to consider*</p>
                        </div>


                        <div class="offset-1 mb-5">
                            <div class="row mb-5 text-left">
                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:100px;border-width: 3px;">Stories</a>

                                <!--<a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3" style="border-color: #000 !important;color:#000;border-radius: 2.2rem;border-width: 2px;width:70px">Apply</a>

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3" style="border-color: #000 !important;color:#000;border-radius: 2.2rem;border-width: 2px;width:170px">Book A Consulation</a> -->
                            </div>

                            <div class="row pt-3">
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #f3dd8a"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #f3dd8a"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #f3dd8a"></span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div id="Private-Direct" class="container pt-5">
            <div class="offset">
                <div class="row pt-5">
                    <div class="col-md-2 p-3"
                         style="background: #4c4c4c;padding-right: 0px !important;margin-bottom:40px;">
                        <h5 class="text-white">Private Direct</h5>
                        <img class="w-100"
                             src="<?php echo esc_url($template_directory_uri . '/wp-img/private-tag.png"'); ?>">
                    </div>
                    <div class="col-md-10">
                        <div class="offset-1 mb-5 ">
                            <h4><b>Overview</b></h4>
                            <p>A Private Direct adoption involves the placement of a child for adoption by their birth
                                family into a family that they know, or even within their own family, such as with a
                                step parent or grandparent. Although these adoptions technically do not require agency
                                or government involvement, they can be complicated and create difficult future
                                relationships. As a result, we recommend that you contact us for a consult to assist
                                with this process if this is something you are considering.</p>
                        </div>


                        <div class="offset-1 mb-5">
                            <div class="row mb-5 text-left">
                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:100px;border-width: 3px;">Stories</a>
                                <!--<a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3" style="border-color: #000 !important;color:#000;border-radius: 2.2rem;border-width: 2px;width:70px">Apply</a> -->

                                <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                                   style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:170px;border-width: 3px;">Book
                                    A Consulation</a>
                            </div>

                            <div class="row pt-3">
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #4c4c4c"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #4c4c4c"></span>
                                <span class="ml-3 mr-3" style="height: 5px;width:28.3333%;background: #4c4c4c"></span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        </div>
    </section>


    <section class="mt-5"
             style="height: 100vh;background-size:90%;background-repeat: no-repeat;background-image: url('<?php echo esc_url($template_directory_uri . '/wp-img/adopting-family.jpg"'); ?>')"
             id="team">

        <div class="row justify-content-start" style="position: relative;top:20%;left:10%">
            <h3 class="text-white pt-5 mb-0 title-tag">You guys all did such an amazing job! </h3>
            <p class="sub-title-tag text-white offset-1" style="top:-20px !important">You went above and beyond for us
                and we really appreciate it!</p>
        </div>
        <!--<div class="row justify-content-end full-width" style="position:relative;top:0%;left:0%;height:100%;width:100%">
  <img style="width:50%;z-index: 1;position:absolute;top:26%;right:-15px;" src="<?php echo esc_url($template_directory_uri . '/wp-img/tag.png"'); ?>">

</div>-->


    </section>

    <section class="shadow" id="team"
             style="background: #202020;padding: 15px;padding-bottom: 60px;width: 90%; margin: 0 0 0 auto;position: relative;top:-200px">
        <!--<div class="row pt-3">
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
            <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
             </div>-->

        <div class="container-fluid ml-0" style="max-width: 900px;padding: 50px 60px">
            <div class="offset">
                <h3 class="text-white mt-2 "><b>Need Help?</b></h3>
                <p class="text-white mt-2 mb-2">Need help determining which adoption option is best for you? Fill out
                    our short simple questionnaire and we’ll help you narrow down the options so you make the best
                    choice for you and your growing family.<br>
                    <span style="color: rgb(243, 221, 138);"><i>Link to survey:</i></span>
                </p>

                <p class="text-white mt-3 mb-2">Once you submit your survey, one of our adoption experts will review it
                    and contact you within (X number) days to give you a recommendation, provide you with more detailed
                    information, and answer your questions.</p>

                <p class="text-white mt-3 mb-2">Or you can book a free consultation now to talk to someone
                    directly. </p>

                <div class="row mb-1 text-left">
                    <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                       style="border-color: #fff !important;color:#fff;border-radius: 2.2rem;width:170px;border-width: 3px;"><b>Learn
                            More</b></a>

                    <!--<a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3" style="border-color: #000 !important;color:#000;border-radius: 2.2rem;border-width: 2px;width:70px">Apply</a> -->

                    <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                       style="border-color: #fff !important;color:#fff;border-radius: 2.2rem;width:170px;border-width: 3px;"><b>Book
                            A Consulation</b></a>
                </div>
            </div>

            <div class="offset mt-5">
                <h3 class="text-white mt-2 "><b>Have Questions?</b></h3>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse1"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How do I know if I’m
                        ready to adopt?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse1" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align"></span>
                </div>

                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse2"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How do I know which
                        option is right for me?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse2" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:44%"></span>
                </div>

                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse3"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Where are you guys
                        located?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse3" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:29%"></span>
                </div>

                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse4"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Where do you have
                        social workers?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse4" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:35%"></span>
                </div>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse5"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">What are the
                        qualifications to adopt?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse5" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align"></span>
                </div>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse6"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">What is the home study
                        and what is the assessment process like?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse6" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:64%"></span>
                </div>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse7"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How much does it cost
                        to adopt?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse7" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:33%"></span>
                </div>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse8"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">How long does it take
                        to adopt?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse8" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:32%"></span>
                </div>

                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse9"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Is there any financial
                        assistance available for adoptive families?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse9" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:62%"></span>
                </div>


                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse10"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Are there age
                        restrictions to adopt?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse10" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:36%"></span>
                </div>

                <div class="groups">
                    <p class="text-white  d-flex mt-1 mb-1" data-toggle="collapse" data-target="#collapse11"
                       aria-expanded="false" aria-controls="collapseTwo" style="cursor: pointer;">Can I adopt as a
                        single?
                        <span class="progress mt-2 ml-5" style="width: 60px;height: 3px;border-radius: 20px"></span></p>
                    <p id="collapse11" aria-labelledby="headingTwo" data-parent="#accordionExample"
                       class="ml-4 mb-3 w-75 collapse" style="color:#f3dd8a">This is the answer to all of lifes toughest
                        questions.it's all here, just have to keep reading on and on.</p>
                    <span class="progress mt-2 ml-5 prog-align" style="left:24%"></span>
                </div>

            </div>

            <div class="offset mt-5">
                <p class="text-white mt-2 mb-2"><b>Download FAQ pdf</p></b>
                <div class="">
                    <div class="progress" style="width: 100px;height: 5px;border-radius: 20px"></div>
                    <div class="progress ml-3 mt-1" style="width: 100px;height: 5px;border-radius: 20px"></div>
                </div>
            </div>


        </div>
    </section>


    <section style="position: relative;top:-100px">


        <div class="container">
            <div class="offset-1">
                <h3 class="mt-4"><b>Want more info?</b></h3>
                <p>“We understand that the adoption process can be overwhelming and daunting. We’re here for you every
                    step of the way. We would be delighted to talk with you and give you more information.” </p>
                <div class="row mb-5 text-center" style="padding: 0px 20px;">
                    <a href="#" class="btn btn-outline-default btn-sm mt-2 ml-3 mr-3"
                       style="border-color: #666 !important;color:#666;border-radius: 2.2rem;width:170px;border-width: 3px;"><b>Book
                            A Consulation</b></a>
                </div>
            </div>
        </div>
    </section>


    <!--==========================
      Footer
    ============================-->
<?php include('footer.php'); ?>