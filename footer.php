<!--==========================
    Footer
  ============================-->
<style>
    .class :hover, .class :focus {
        color: #db6680 !important;

    }


</style>
<footer id="footer" class="max-height"
        style=" background-image:url('<?php echo esc_url($template_directory_uri . '/wp-img/footer-bg.jpg"'); ?>');background-size:cover;">
    <div class="footer-top" style="background: transparent;">
        <div class="container">
            <div class="row justify-content-center mt-1">

                <img style="height: 70px;"
                     src="<?php echo esc_url($template_directory_uri . '/wp-img/footer-logo.png"'); ?>">


                <!-- <div class="col-lg-3 col-md-6 footer-info">
                   <h3>Amaris</h3>
                   <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                 </div>

                 <div class="col-lg-3 col-md-6 footer-links">
                   <h4>Useful Links</h4>
                   <ul>
                     <li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
                     <li><i class="ion-ios-arrow-right"></i> <a href="#">About us</a></li>
                     <li><i class="ion-ios-arrow-right"></i> <a href="#">Services</a></li>
                     <li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
                     <li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
                   </ul>
                 </div>

                 <div class="col-lg-3 col-md-6 footer-contact">
                   <h4>Contact Us</h4>
                   <p>
                     A108 Adam Street <br>
                     New York, NY 535022<br>
                     United States <br>
                     <strong>Phone:</strong> +1 5589 55488 55<br>
                     <strong>Email:</strong> info@example.com<br>
                   </p>

                   <div class="social-links">
                     <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                     <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                     <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                     <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                     <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                   </div>

                 </div>

                 <div class="col-lg-3 col-md-6 footer-newsletter">
                   <h4>Our Newsletter</h4>
                   <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
                   <form action="" method="post">
                     <input type="email" name="email"><input type="submit"  value="Subscribe">
                   </form>
                 </div>-->

            </div>

            <div class="row justify-content-center mt-5">
                <header id="header" class="" style="position: static;height:100%">
                    <div class="container-fluid">

                        <nav id="nav-menu-container pr-0">
                            <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
                                <li class=""><a href="<?php echo site_url($path, $scheme); ?>/index">Home</a></li>
                                <li><a href="<?php echo site_url($path, $scheme); ?>/about">About</a></li>
                                <li><a href="<?php echo site_url($path, $scheme); ?>/adopting">Adopting</a></li>
                                <li><a href="<?php echo site_url($path, $scheme); ?>/expecting">expecting</a></li>
                                <li><a href="<?php echo site_url($path, $scheme); ?>/story">stories</a></li>
                                <li><a href="<?php echo site_url($path, $scheme); ?>/family-support">family Support</a>
                                </li>
                            </ul>
                        </nav><!-- #nav-menu-container -->
                    </div>
                </header>
            </div>

            <div class="row justify-content-center mt-2">
                <div class="social-links">
                    <a href="#" class="facebook icon bg-white" style="color:#c67f8f;"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="instagram icon bg-white" style="color:#c67f8f;"><i
                                class="fa fa-instagram"></i></a>


                </div>

            </div>
            <div class="row justify-content-center mt-5">

                <p class="text-center p-4">AMARIS 2018 | 201B 9705 Horton Rd S.W. Calgary, AB, T2V 2X5 | 403.256.3224 |
                    Info@amarisadoption.com
                </p>
            </div>
        </div>
    </div>


</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="<?php echo esc_url($template_directory_uri . '/lib/jquery/jquery.min.js"'); ?>"></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/jquery/jquery-migrate.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/bootstrap/js/bootstrap.bundle.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/easing/easing.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/superfish/hoverIntent.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/superfish/superfish.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/wow/wow.min.js"'); ?>  "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/waypoints/waypoints.min.js"'); ?>  "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/counterup/counterup.min.js"'); ?>  "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/owlcarousel/owl.carousel.min.js"'); ?>  "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/isotope/isotope.pkgd.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/lightbox/js/lightbox.min.js"'); ?> "></script>
<script src="<?php echo esc_url($template_directory_uri . '/lib/touchSwipe/jquery.touchSwipe.min.js"'); ?> "></script>
<!-- Contact Form JavaScript File -->
<script src="<?php echo esc_url($template_directory_uri . '/contactform/contactform.js"'); ?> "></script>

<!-- Template Main Javascript File -->
<script src="<?php echo esc_url($template_directory_uri . '/js/main.js"'); ?> "></script>

