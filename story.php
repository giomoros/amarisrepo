<?php /* Template Name: story */ ?>
<?php include('header.php'); ?>
<body style="overflow-x: hidden;">

<?php include('navbar.php'); ?>
  <!--==========================
    Intro Section

  ============================-->

  

<section class="header-position" id="team" style="background-image:url('<?php echo esc_url( $template_directory_uri . '/wp-img/family-2.jpg"' ); ?>');">

    <div class="container" style="">
      <div class="centered text-center" style="left: unset;transform: translate(0%, 0%);width:82%;">
        <h3 class="text-white  title-tag" style="font-size: 60px;">Adoption <span class="text-white sub-title-tag " style="font-size: 60px;">Stories</span></h3>

       
      </div>
    </div>
      <div class="row pt-3 justify-content-end" style="position: absolute;bottom: -5px;width:100%;left:0px">
          <span class="ml-3 mr-3 hb-1"></span>
         <span class="ml-3 mr-3 hb-2"></span>
         <span class="ml-3 mr-3 hb-3"></span>
          </div>
          
  </section><!-- #intro -->




  <section id="team" style="background: #202020;padding: 15px;padding-bottom: 20px;width: 90%;">
     <!--<div class="row pt-3">
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 5px;width:16.6666%;background: #f3dd8a"></span>
       </div>-->

       <div class="container-fluid" style="max-width: 800px;padding: 50px 0px">
        <div class="offset">
          <h3 class="text-white mt-2 ">Hear Stories <span class="sub-title-tag">From</span></h3>
        </div>

      </div>
    </section>



  <section id="call-to-action" style="background-image: none;width:80%;margin:0 auto; ">
    <div class="container">
    <div class="row">
      <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s">
            <div class="member">
              <img style="width:100%;object-fit: cover" src="<?php echo esc_url( $template_directory_uri . '/wp-img/divi-1.png"' ); ?>" class="img-fluid" alt="">
              <div class="centered" style="left:46%">
                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">International Adoption</p>
              </div>
              </div>
            </div>

            <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s">
            <div class="member">
              <img style="width:100%;object-fit: cover" src="<?php echo esc_url( $template_directory_uri . '/wp-img/divi-2.png"' ); ?>" class="img-fluid" alt="">
              <div class="centered" style="left:46%">
                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Domestic Adoption</p>
              </div>
              </div>
            </div>

            <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s">
            <div class="member">
              <img style="width:100%;object-fit: cover" src="<?php echo esc_url( $template_directory_uri . '/wp-img/divi-3.png"' ); ?>" class="img-fluid" alt="">
              <div class="centered" style="left:46%">
                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Government Adoption</p>
              </div>
              </div>
            </div>

            <div class="col-md-3 wow fadeInUp pr-0 pl-0" data-wow-delay="0.2s">
            <div class="member">
              <img style="width:100%;object-fit: cover" src="<?php echo esc_url( $template_directory_uri . '/wp-img/divi-4.png"' ); ?>" class="img-fluid" alt="">
              <div class="centered" style="left:46%">
                <p class="text-white" style="text-align:left;font-size:22px;font-weight: 700">Private Direct</p>
              </div>
              </div>
            </div>


    </div>
    </div>
  </section>

<section style="height: 100vh;background-size:cover;background-image: url('<?php echo esc_url( $template_directory_uri . '/wp-img/family-3.jpg"' ); ?>')" id="team">

  <div class="row justify-content-end full-width" style="position:relative;top:18%;left:0%;height:100%;width:100%">
  <img style="width:35%;z-index: 1;position:absolute;top:44%;right:-15px;" src="<?php echo esc_url( $template_directory_uri . '/wp-img/tag.png"' ); ?>">

</div>

</section>





<section id="team" style="padding: 90px 0;background: #202020;height:40%;width: 90%; margin: 0 0 0 auto;position: relative;top:-80px">
    

    <div class="row pt-3 justify-content-start" style="position: absolute;bottom: -5px;width:100%;left:0px">
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #8c5776"></span>
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #fbb26a"></span>
         <span class="ml-3 mr-3" style="height: 10px;width:16.6666%;background: #f3dd8a"></span>
          </div>
  

  </section>



 <?php include('footer.php'); ?>